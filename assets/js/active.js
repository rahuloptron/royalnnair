/* jshint esnext: true */
var document,
    window;
(function ($) {
    'use stict';
    const dh = {};
    // preloader
    dh.fnPreloader = () => {
        dh.preloader = $('.preloader');
        if (dh.preloader.length) {
            dh.preloader.fadeOut(1000);
        }
    };

    dh.customCode = () => {
        dh.searchIcon = $('.search-icon');
        dh.searchForm = $('.search-box');
        dh.searchClose = $('.search-close');
        dh.searchIcon.add(dh.searchClose).on('click', function (e) {
            e.preventDefault();
            dh.searchForm.toggleClass('active');
        });

        // dynamic-margin-padding
        var dataMargin = $('[data-margin]');
        dataMargin.each(function () {
            var getMargin = $(this).attr('data-margin');
            $(this).css('margin', getMargin);
        });
        var dataPadding = $('[data-padding]');
        dataPadding.each(function () {
            var getPadding = $(this).attr('data-padding');
            $(this).css('padding', getPadding);
        });
    };

    dh.fnPlugins = () => {

        dh.mainMenu = $('.navigation--menu > ul');
        if ($.fn.slicknav) {
            dh.mainMenu.slicknav();
        }

        if ($.fn.stellar) {
            $(window).stellar();
        }

        dh.footerGrid = $('.footer-grid');
        if ($.fn.isotope) {
            dh.footerGrid.isotope({
                itemSelector: '.footer-grid .widget',
                layoutMode: 'masonry'
            });
        }

        dh.postGrid = $('.post-grid');
        if ($.fn.isotope) {
            dh.postGrid.isotope({
                itemSelector: '.single-post',
                layoutMode: 'masonry'
            });
        }

        if ($.fn.YTPlayer) {
            $('#home-video').YTPlayer();
        }

        dh.videoPopup = $('.video-popup');
        if ($.fn.magnificPopup) {
            dh.videoPopup.magnificPopup({
                disableOn: 0,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: true,
                fixedContentPos: false
            });
        }

    };

    dh.fnSlider = () => {
        if ($.fn.owlCarousel) {

            dh.owlClass = 'owl-carousel';

            //homeSlider
            dh.homeSlider = $('.home-slider');
            if (dh.homeSlider) {
                dh.homeSlider.addClass(dh.owlClass);
                dh.homeSlider.owlCarousel({
                    items: 1,
                    dots: false,
                    nav: true,
                    loop: true,
                    autoplay: true,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                });
            }

            //testimonial
            dh.testimonialSlider = $('.testimonial-slider');
            if (dh.testimonialSlider) {
                dh.testimonialSlider.addClass(dh.owlClass);
                dh.testimonialSlider.owlCarousel({
                    items: 3,
                    dots: false,
                    margin: 30,
                    loop: true,
                    autoplay: true,
                    nav: true,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    responsive: {
                        0: {
                            items: 1
                        },
                        992: {
                            items: 2
                        },
                        1200: {
                            items: 3
                        }
                    }
                });
            }

            //brand-slider
            dh.brandSlider = $('.brand-slider');
            if (dh.brandSlider) {
                dh.brandSlider.addClass(dh.owlClass);
                dh.brandSlider.owlCarousel({
                    items: 5,
                    dots: false,
                    margin: 30,
                    loop: true,
                    autoplay: true,
                    nav: true,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    responsive: {
                        0: {
                            items: 2
                        },
                        480: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        992: {
                            items: 5
                        }
                    }
                });
            }
        }

        if ($.fn.slick) {
            dh.portfolio_slider = $('.portfolio-slider');
            dh.portfolio_slider.slick({
                slidesToShow: 3,
                variableWidth: true,
                centerMode: true,
                prevArrow: '<i class="prev-arrow fa fa-long-arrow-left"></i>',
                nextArrow: '<i class="next-arrow fa fa-long-arrow-right"></i>'
            });
        }
    };

    dh.ajaxPortfolio = () => {


        dh.portfolio_isotope = $('.portfolio-isotope');
        if ($.fn.isotope) {
            dh.portfolio_isotope.isotope({
                itemSelector: '.single-portfolio-1',
                layoutMode: 'masonry'
            });
        }
        
        dh.portBtn = $('.port-btn');
        dh.portBtn.on('click', function (e) {
            e.preventDefault();
            $(this).html('loading <i class="fa fa-spinner fa-pulse"></i>');
            setTimeout(function () {
                $.ajax({
                    url: 'assets/js/portfolio.json',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        id: "nav"
                    },
                    complete: function (xhr, textStatus) {
                        var portData = xhr.responseJSON.portfolios;
                        for (var i = 0; i < portData.length; i++) {
                            var portList = portData[i];
                            var portfolioMarkup = '';
                            portfolioMarkup += '<div class="col-md-4 single-portfolio-1">';
                            portfolioMarkup += '<a href="single-portfolio.php" class="inner">';
                            portfolioMarkup += '<div class="portfolio-img">';
                            portfolioMarkup += '<img src="' + portList.src + '" alt="">';
                            portfolioMarkup += '</div>';
                            portfolioMarkup += '<div class="portfolio-content">';
                            portfolioMarkup += '<h4>' + portList.title + '</h4>';
                            portfolioMarkup += '<span>' + portList.cat + '</span>';
                            portfolioMarkup += '</div>';
                            portfolioMarkup += '</a>';
                            portfolioMarkup += '</div>';
                            
                            $('.portfolio-isotope').append(portfolioMarkup);
                            dh.portfolio_isotope.isotope('reloadItems');
                            dh.portfolio_isotope.isotope({
                                itemSelector: '.single-portfolio-1',
                                layoutMode: 'masonry'
                            });
                            dh.portBtn.html('load more <i class="fa fa-check"></i>');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }, 1000);

        });
    };
    // all functions of document ready
    dh.docReady = () => {
        dh.fnPreloader();
        dh.fnSlider();
        dh.fnPlugins();
        dh.customCode();
    };

    // all functions of window load
    dh.winLoad = () => {
        dh.fnPreloader();
        dh.ajaxPortfolio();
    };


    $(document).ready(dh.docReady);
    $(window).on('load', dh.winLoad);



})(jQuery); // jshint ignore:line
